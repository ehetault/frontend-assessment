# Front-end assessment


## Problem statement


Your task is to implement the following layout, using whichever tools and libraries you see fit.


Desktop:

![Desktop layout](./desktop.png)

Mobile:

![Mobile layout](./mobile.png)

### Header
The header should be fullscreen in such a way, that the diagonal shapes are not visible, but start immediately below the visible area.

The background consists of a video, which plays automatically as soon as the page loads. There should be no controls visible, the video should be muted, and it should play on loop.

Video source: https://www.youtube.com/watch?v=nH4Z0TlYiio

### Content
The content contains a grid of images that can be filtered by type. By default, the filter "All" is active, and all images are displayed. When a user clicks on "Computer" or "Pen & Paper", only images from the respective category should be shown.

On desktop, when a user moves the mouse over an image, the image should zoom in, the overlay should change its color, and a title should appear:
default | hover
--- | ---
![Grid image](./image.png) | ![Grid image hover](./image-hover.png)

You can find the images in ../images


### Design

#### Grid and measurements
The web site should always be full width.

The designer has provided you with some basic measurements. Unfortunately, they are incomplete. You are expected to perform the missing measurements yourself.

![Desktop layout measurements](./desktop-measurements.png)

![Mobile layout measurements](./mobile-measurements.png)

#### Fonts
The fonts used in this layout are Open Sans bold and regular.

You can find the fonts in ../fonts

#### Colors
* header overlay: #225FA5, opacity: 70%
* content area background: #225FA5
* light blue diagonal shape at the top right of the content area: #669AD7
* grid image overlay: gradient from #1E4778, opacity 30% (top) to #1E4778, opacity 50% (bottom)
* grid image overlay hover: gradient from #B5D7FF, opacity 70% (top) to #B5D7FF, opacity 95% (bottom)
* grid image title: #1E4778
* grid filter active: #225FA5, background: #fff
* grid filter inactive: #fff, opacity: 50%

## Solution
Please set up a new git repository (e.g. on GitHub or Bitbucket) and commit your solution to that repository. Once you are done, please drop us a mail at development@dimando.com with the link to your repository.
